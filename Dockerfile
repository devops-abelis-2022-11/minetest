FROM debian:latest as BUILD

RUN apt update && apt install -y \
    g++  \
    make  \
    libc6-dev  \
    cmake  \
    libpng-dev \
    libjpeg-dev \
    libxi-dev  \
    libgl1-mesa-dev  \
    libsqlite3-dev  \
    libogg-dev  \
    libvorbis-dev  \
    libopenal-dev  \
    libcurl4-gnutls-dev  \
    libfreetype6-dev zlib1g-dev libgmp-dev libjsoncpp-dev  \
    libzstd-dev libluajit-5.1-dev \
    git

RUN git clone --depth=1 https://github.com/minetest/irrlicht lib/irrlichtmt
WORKDIR lib/irrlichtmt
RUN cmake . -DBUILD_SHARED_LIBS=OFF
RUN make -j$(nproc)

RUN git clone https://github.com/minetest/minetest/ /minetest
RUN git clone https://github.com/minetest/minetest_game /minetest/games/minetest_game

WORKDIR /minetest
RUN cmake . -DRUN_IN_PLACE=TRUE -DBUILD_SERVER=TRUE -DBUILD_CLIENT=FALSE
RUN make -j $(nproc)

FROM debian:11.5-slim

RUN apt update && apt install -y \
    libc6-dev  \
    libpng-dev \
    libjpeg-dev \
    libxi-dev  \
    libgl1-mesa-dev  \
    libsqlite3-dev  \
    libogg-dev  \
    libvorbis-dev  \
    libopenal-dev  \
    libcurl4-gnutls-dev  \
    libfreetype6-dev zlib1g-dev libgmp-dev libjsoncpp-dev  \
    libzstd-dev libluajit-5.1-dev

COPY --from=BUILD /minetest/bin/minetestserver /minetest/bin/minetestserver
COPY --from=BUILD /minetest/builtin /minetest/builtin
COPY --from=BUILD /minetest/games /minetest/games
COPY --from=BUILD /minetest/worlds /minetest/worlds

WORKDIR /minetest/bin

CMD ["/minetest/bin/minetestserver"]

VOLUME /minetest/worlds

EXPOSE 30000/udp
